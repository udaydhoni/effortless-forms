export interface AddedInputInfo  {
    inputType:string;
    options?: string [];
    subsectionIdentifier : string | number;
    pageIdentifier : string | number;
    staticValidations?: string [];
    questionTag: string;
    dynamicValidation:boolean;
  }

export interface Field {
    type:string;
    staticValidations: string [];
    questionTag: string;
    dynamicValidation: boolean;
    initialValue: string | number;
    separateField: boolean;
    options?: string [];
    style:string;
}

export interface Subsection {
    title:string;
    isDuplicate:boolean;
    fields: Field [];
    crossFieldValidations: string[];
    maxCount: number;
    insuffMesssage: string;
}

export interface PageData {
    pageTitle:string;
    subsections: Subsection [];
    crossSubsectionValidations: string [];
}