import { Component,Output,EventEmitter } from '@angular/core';
import { AddedInputInfo } from '../utility.interfaces';
import {FormBuilder} from '@angular/forms'

interface validationType{
  name: string,
  descriptiveName: string
}
@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent {

  constructor (fb: FormBuilder ) {

  }
  
  @Output() newInputEvent = new EventEmitter<AddedInputInfo>();

  typeOfValidations : validationType [] = [{name : "required",descriptiveName: "Required"},{name:"min",descriptiveName: "Minimum Number"}]
  public debug () {
    console.log("The button is clicked");
  }
  public addNormalInput (fieldToBeAdded: AddedInputInfo) {
    this.newInputEvent.emit(fieldToBeAdded);
  }

  
}
